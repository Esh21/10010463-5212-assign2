﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace COMP._5212_10010463
{
    public class Customer
    {
        public string FName

        {
            get; set;
        }
        public string LName
        {
            get; set;
        }
        public string Phone
        {
            get; set;
        }

        public Customer(string fn, string ln, string ph)
        {
            FName = fn;
            LName = ln;
            Phone = ph;



        }


        public string GetCustomer()
        {
            return FName + LName + Phone;

        }
    }

    public partial class MainWindow : Window
    {

        /// <summary>
        /// Interaction logic for MainWindow.xaml
        /// </summary>

        private static List<Customer> CustomerDB = new List<Customer>();
        private void LoadDB()
        {
            CustomerDB.Add(new Customer("Jaarna", "Kereopa", "123-2514"));
            CustomerDB.Add(new Customer("Sue", "Stook", "123-1263"));
            CustomerDB.Add(new Customer("Jamie", "Allom", "123-3658"));
            CustomerDB.Add(new Customer("Brian", "Janes", "123-1263"));

            NameBox.ItemsSource = CustomerDB;


        }


        private void ClearBoxes()
        {
            firstName.Clear();
            lastName.Clear();
            phone.Clear();

        }
        private void DisplayCustomer()
        {
            NameBox.ItemsSource = CustomerDB;
        }
        private void ClearDisplay()
        {
            NameBox.ItemsSource = null;
            NameBox.Items.Clear();
        }

        public MainWindow()
        {
            InitializeComponent();

            DisplayCusomer();


        }
        private void DisplayCusomer()
        {

            LoadDB();
        }



        private void add_Click(object sender, RoutedEventArgs e)
        {


            int num = -1;


            if (firstName.Text == "" | lastName.Text == "" | phone.Text == "")
            {
                MessageBox.Show("All textboxes must be filled to continue.");

            }
            else if (!int.TryParse(phone.Text, out num))
            {
                MessageBox.Show("Please input number");
            }

            else
            {



                CustomerDB.Add(new Customer(firstName.Text, lastName.Text, phone.Text));


                //LoadDB();
                MessageBox.Show("New customer has been added");
                //NameBox.ItemsSource = CustomerDB;
                ClearDisplay();
                DisplayCustomer();


            }

        }


        private void listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DisplayCusomer();


        }

        private void listCust_Click(object sender, RoutedEventArgs e)
        {
            DisplayCusomer();
        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            ClearBoxes();
        }

        private void clearList_Click(object sender, RoutedEventArgs e)
        {
            ClearDisplay();
        }

        private void searchbtn_Click(object sender, RoutedEventArgs e)
        {
            NameBox.ItemsSource = CustomerDB;
            var items = NameBox.Items;

            List<Customer> p = new List<Customer>();
            foreach (Customer item in items)
            {
                if (item.FName == search.Text)
                {
                    p.Add(item);
                 

                }

                NameBox.ItemsSource = p;
            }
            if (search.Text == "")
            {
                MessageBox.Show("Please enter a name to show");
                NameBox.ItemsSource = search.Text;


            }
        }

    


        private void update_Click(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;
            var name = NameBox.Items;
            string test1 = name.ToString();
            // string[] array1 ={ FName, LName, Phone };
           

            // Customer[] myArray = CustomerDB.ToArray();
            if (textBox ==  null)
            {

                NameBox.ItemsSource = firstName.Text;
                
            }
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            //  var x = MessageBox.Show("Do you really want to delete", "ListBoxDemo", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            //  NameBox.Items.RemoveAt

           
            

            }
           

        }


    }

